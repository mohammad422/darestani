<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/username', 'UserController@index')->name('username');

Route::get('/user/{username?}', 'UserController@action')->name('user1');

Route::get('/user/{post1?}/{post2?}/{post3?}','UserController@action2')->name('user2');

Route::get ('/users','UserController@action3')->name('users');

Route::get ('/userpath','UserController@extract')->name('userpath');

Route::get('/readfile','UserController@extract2')->name('readfile');

Route::get('/readfile1','UserController@extract3')->name('readfile1');

Route::get('/routename/{name}/{width?}/{height?}','UserController@namedRoute')->name('routename');

Route::get('form','FormController@viewForm');

Route::post('show','FormController@showName')->name('show');

Route::get('file','FormController@viewFileForm')->name('file');

Route::post('back','FormController@backToForm')->name('back');

Route::get('layout','FormController@showLayout')->name('layout');

Route::post('upload','FormController@uploadFile')->name('upload');

Route::get('array','FormController@showArray')->name('array');

Route::get('htmlfile','FormController@downloadHtml')->name('htmlfile');

//route for practice 1
Route::get('/excel','ExcelController@readFile')->name('excel');

//route for practice 2
Route::get('/image/{image_name}/{width?}/{height?}','ImageController@imgResize')->name('image');

Route::get('/fa','UserController@langFa')->name('fa');

Route::get('/setsess','UserController@setSession')->name('setsess');

Route::get('/getsess','UserController@getSession')->name('getsess');

Route::get('/resume','ResumeController@index')->name('resume');

Route::post('/showresume','ResumeController@show')->name('show.resume');

Route::get('/employees','FormController@index')->name('employees');

Route::get('/employees/{id}','FormController@show')->name('employee_id');

//Route::get('/viewFiles','FormController@uploadFile')->name('viewFiles');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/resumes','ResumeController@getAllResumes')->name('resumes');

Route::get('/downloadResume/upload/{fakename}','ResumeController@downloadFile')->name('download');

Route::get('/gallery','FormController@showGallery')->name('gallery');

Route::get('/editfile/{id}','FormController@editFile')->name('edit.file');

Route::post('/edit','FormController@doEdit')->name('edit');

Route::get('/deletefile/{id}','FormController@deleteFile')->name('delete.file');

Route::get('/fileDetails/{id}','FormController@showFile')->name('details');

Route::post('/comment/{id}','FormController@sendComment')->name('comment');
/*
Route::middleware(['auth'])->group(function () {
    Route::get('/');
    Route::get('/resumes');
    Route::get('/employees');
    Route::get('/employees/{id}');
    Route::get('/downloadResume/upload/{fakename}','ResumeController@downloadFile')->name('download');
});*/
Route::get('/comments','CommentController@index')->name('comments');

Route::post('/tag/{id}','TagController@sendTag')->name('tag');

Route::get('/postDetails','PostController@showPost');

Route::get('/galleryAd/{file}','FormController@gallery')->name('isAdmin');

