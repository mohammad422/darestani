<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'گذرواژه :name تنظیم مجدد شد.',
    'sent' => 'ما لینک بازیابی گذروازه شما را ایمیل کردیم.',
    'token' => 'این توکن بازیابی رمز عبور نامعتبر است.',
    'user' => "ما نمیتوانیم یک ماربر با این ایمیل پیدا کنیم.",
    'throttled' => 'لطفا بعد از تاش مجدد منتظر بمانید ',

];
