
@extends('layouts.app1')
@section('title', 'form')
@section('stylesheets')
    @parent
@endsection
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    @foreach($employees as $employee)

        <p><a href="/employees/{{$employee['id']}}">{{$employee['username']}}</a></p>

    @endforeach
    {{ $employees->links() }}
@endsection
