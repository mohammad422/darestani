
@extends('layouts.app1')
@section('title', 'fileform')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h2><a href="/gallery">Gallery</a></h2>
    {{session('user.welcome')}}
    @foreach ($file->tags as $tag)
        <p>{{$tag->name}} Priority:{{$tag->pivot->priority}}</p>


    @endforeach
    <div class="col-sm-4">
        <form method="post" action="{{route('edit')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$file->id}}">
            <p>{{$file->filename}}</p>
            <div class="form-group">
                <h3>Description</h3>
                <textarea  name="description">{{$file ->description}}</textarea>
            </div>
            <input type="hidden" name="id" value="{{$file->id}}">

            @foreach ($tags as $tag)
                <p>
                <input type="checkbox" name="tags[]" value="{{$tag->id}}"><span>{{$tag->name}}</span>
                <input type="number" name="priority[]" >
                </p>
            @endforeach

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection
