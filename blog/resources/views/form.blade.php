@extends('layouts.app1')
@section('title', 'form')
@section('stylesheets')
    @parent
@endsection
@section('sidebar')
    @parent


@endsection

@section('content')


    @foreach($errors as $user)
        @foreach($user as $item)
           {{$item}}
         @endforeach
        @endforeach
    <div class="col-sm-4">
<form method="post" action="{{route('back')}}">

        @csrf

    <div class="form-group">
        <label for="username1">Username</label>
        <input type="text" name="username" class="form-control" id="username" aria-describedby="usernameHelp" placeholder="Enter username">

    </div>
    <div class="form-group">
        <label for="email">Email address</label>
        <input type="email" name="email" class="form-control" id="password" aria-describedby="emailHelp" placeholder="Enter email">

    </div>
    <div class="form-group">
        <label for="tel">Tel</label>
        <input type="tel" name="tel" class="form-control" id="tel" aria-describedby="emailHelp" placeholder="Enter tel">

    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
            <button type="submit" class="btn btn-primary">Submit</button>

</form>
    </div>
@endsection
