
@extends('layouts.app1')
@section('title', 'form')
@section('stylesheets')
    @parent
@endsection


@section('content')
<div style="margin-top: 100px;">
    <table class="table table-striped ">
        <thead class="thead-dark">
        <tr>
            <th scope="col">FirstName</th>
            <th scope="col">LastName</th>
            <th scope="col">Email</th>
            <th scope="col">Tel</th>
            <th scope="col">Skills</th>
            <th scope="col">Resume</th>
        </tr>
        </thead>
        <tbody>
        @foreach($resumes as $resume)
        <tr>
            <th scope="row">{{$resume['firstname']}}</th>
            <td>{{$resume['lastname']}}</td>
            <td>{{$resume['email']}}</td>
            <td>{{$resume['tel']}}</td>
            <td>{{$resume['skill']}}</td>
            <td><a href="/downloadResume/{{$resume['fakename']}}">{{$resume['file']}}</a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection
