@extends('layouts.app1')
@section('title', 'comments')

@section('sidebar')
    @parent


@endsection

@section('content')
    <h2>All Comments</h2>
    @foreach($comments as $comment)
        <div class="col-6" style="margin-top: 10px;">
        <table>
            <tr>
                <td></td>
            </tr>
        </table>
            <p>{{$comment->comment}}</p>
               @if($comment->type === 'App\File')

               <p><a href="/fileDetails/{{$comment->commentable->id}}">{{$comment->commentable->filename}}</a></p>

                   @elseif($comment->type ==='App\Post')

                <p><a href="/fileDetails/{{$comment->commentable->id}}">{{$comment->commentable->title}}</a></p>

                   @endif


        </div>

    @endforeach
@endsection
