@extends('layouts.app1')
@section('title', 'fileform')

@section('sidebar')
    @parent


@endsection

@section('content')
    <form method="get" action="/gallery">
        <input type="checkbox" name="hascomment">
        <input type="submit">
    </form>
    <a href="/hascomment" class="btn-danger-lg" >File with comments</a>
@foreach($files as $file)
<div class="col-6" style="margin-top: 10px;">
    @foreach ($file->tags as $tag)
     {{$tag->name}}{{$tag->pivot->priority}}

    @endforeach
   <img src="{{asset('/storage/'.$file['fakename'])}}">
    <p><a href="/fileDetails/{{$file['id']}}">{{$file['filename']}}</a></p>
    <p>{{$file['description']}}</p>

    <a href="/editfile/{{$file['id']}}" class="btn-warning">Edit</a>
    <a href="/deletefile/{{$file['id']}}" class="btn-danger">Delete</a>
    <div class="card">
        <div class="card-body">
            @foreach($file->comments as $value )
                <p>{{$value->comment}}</p>
                @endforeach
        </div>
    </div>
</div>
@endforeach
@endsection
