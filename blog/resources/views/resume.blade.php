@extends('layouts.app1')
@section('title', 'form')
@section('stylesheets')
    @parent
@endsection
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h3 style="text-align: center;"><a href="/resumes">The List of Resumes</a></h3>
    <div class="col-md-6 offset-md-3" style="margin-top: 50px;border:3px solid grey;border-radius: 5px;padding-bottom:50px; padding-top:50px; ">
        <h2>Insert Your Information</h2>
        <form method="post" action="{{route('show.resume')}}" enctype="multipart/form-data">

            @csrf

            <div class="form-group">
                <label for="firstname">FirstName</label>
                <input type="text" name="firstname" value="{{old('firstname')}}" class="form-control" id="firstname" aria-describedby="firstnameHelp" placeholder="Enter firstname">
                @error('firstname')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="lastname">LastName</label>
                <input type="text" name="lastname" value="{{old('lastname')}}" class="form-control" id="lastname" aria-describedby="lastnameHelp" placeholder="Enter lastname">
                @error('lastname')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="email">Email address</label>
                <input type="email" name="email" value="{{old('email')}}" class="form-control" id="password" aria-describedby="emailHelp" placeholder="Enter email">
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="tel">Tel</label>
                <input type="tel" name="tel" value="{{old('tel')}}" class="form-control" id="tel" aria-describedby="emailHelp" placeholder="Enter tel">

            </div>
            <div class="form-check">
                <h3>Skills</h3>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="skill[]" value="Php" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        Php
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="skill[]" value="Javascript" id="defaultCheck2">
                    <label class="form-check-label" for="defaultCheck2">
                        Javascript
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="skill[]" value="Java" id="defaultCheck3">
                    <label class="form-check-label" for="defaultCheck3">
                        Java
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="skill[]" value="Python" id="defaultCheck4">
                    <label class="form-check-label" for="defaultCheck4">
                        Python
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="skill[]" value="Wordpress" id="defaultCheck5">
                    <label class="form-check-label" for="defaultCheck5">
                        Wordpress
                    </label>
                </div>
                @error('skill')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group">
                <label for="file">Resume File</label>
                <input type="file" name="file" class="form-control" id="file" aria-describedby="fileHelp" placeholder="Enter file">
                @error('file')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
@endsection
