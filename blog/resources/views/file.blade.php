
@extends('layouts.app1')
@section('title', 'fileform')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')
    <h2><a href="/gallery">Gallery</a></h2>
{{session('user.welcome')}}
    <div class="col-sm-4">
        <form method="post" action="{{route('upload')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="File1">Example file input</label>
                <input type="file" name="file" class="form-control-file" id="File1">
            </div>
            <div class="form-group">
                <h3>Description</h3>
                <textarea  name="description"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

@endsection
