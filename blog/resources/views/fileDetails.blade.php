@extends('layouts.app1')
@section('title', 'fileform')

@section('sidebar')
    @parent


@endsection

@section('content')

        <div class="col-6" style="margin-top: 10px;">

            <p>{{$file->filename}}</p>
            <p>{{$file->description}}</p>

            <div class="col-12">
                @foreach($file->comments as $comment)
                    <div class="col-4 " style="border:1px solid black;text-align: center;padding-top: 0px;">
                       <p>{{$comment->comment}}</p>
                    </div>
                    @endforeach
            </div>
        </div>
        <div>
            <h2>Add Commments</h2>
            <form method="post" action="/comment/{{$file->id}}">
                @csrf
                <input type="hidden" name="id" value="{{$file->id}}">
                <textarea name="comment"></textarea>
                <input type="submit">
            </form>
        </div>
        <div>
            <h2>Add Tags</h2>
            <form method="post" action="/tag/{{$file->id}}">
                @csrf
                <input type="hidden" name="id" value="{{$file->id}}">

                        @foreach ($tags as $tag)
                            <input type="checkbox" name="tags[]" value="{{$tag->id}}"><span>{{$tag->name}}</span>
                        @endforeach

                <button class="btn-save btn btn-primary btn-sm">Save</button>
            </form>
        </div>
@endsection
