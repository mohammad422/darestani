<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'files';
   // public $timestamps = false;
    protected $fillable = ['filename','fakename','description','user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
//    public function comments()
//    {
//        return $this->hasMany(Comment::class,'file_id','id');
//    }


    /**
     * Get all of the post's comments.
     */
    public function comments()
    {

        return $this->morphMany(Comment::class, 'commentable','type','type_id');
    }

   /* public function tags()
    {
        return $this->belongsToMany(Tag::class,'files_has_tags','file_id','tag_id')->withPivot(['priority']);
    }*/
    /**
     * Get all of the tags for the post.
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class , 'taggable','files_has_tags');
    }

}
