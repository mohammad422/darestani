<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Employee;

use App\File;
use App\Tag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Matrix\Builder;
use function foo\func;

class FormController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewForm()
    {
        return view('form');
    }

    public function backToForm(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'tel' => 'required',
            'email' => 'required|email:rfc',
            'password' => 'required|alpha_dash|min:5|max:10',
        ]);

        Employee::create($request->except('_token'));
        dd($request->all());
        //return back();

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $employees = Employee::paginate(5);

        //  dd($employees);
        return view('employee', compact('employees'));
    }

    public function show($id)
    {
        dd($id);
        $employee = Employee::find($id);
        //  dd($employee);
        // $employee = \App\Employee::findOrFail($id);
        return view('employeeshow', compact('employee'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewFileForm()
    {

        return view('file');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showName()
    {
        if (isset($_POST['name'])) {
            $name = $_POST['name'];
        } else {
            $name = null;
        }
        return view('user', ['name' => $name]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLayout()
    {
        return view('layouts/app');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function uploadFile(Request $request)
    {
        $attribute = $request->except(['_token']);

        $attribute['filename'] = $request->file('file')->getClientOriginalName();
        $attribute['fakename'] = $request->file('file')->store('public');
        $attribute['user_id'] = \Auth::id();

        File::query()->create($attribute);
        return redirect()->route('gallery');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showGallery(Request $request)
    {
        // $files = File::all();
        /* $newfile=new File();
         dd($newfile->comments());*/
        /* $newcom=new Comment();
         dd($newcom->commentable());*/
        $newtag = new Tag();
        // dd($newtag->files());
        $files = File::query()
            ->with('comments')->with('tags')
            ->when($request->has('hascomment'), function (\Illuminate\Database\Eloquent\Builder $query) {
                $query->whereHas('comments', function (\Illuminate\Database\Eloquent\Builder $query) {
                    $query->where('created_at', '>', Carbon::yesterday());
                });
            }
            )->get();

        /* if($request->has('hascomment')){
             $file = File::query()->has('comments')->with('comments');
         }*/

        /* dd($files);*/
        return view('gallery', compact('files'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showFile($id)
    {

        $file = File::query()->with('comments')->with('tags')->where('id', $id)->first();

        $tags = Tag::query()->get();
        return view('fileDetails', compact(['file', 'tags']));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editFile($id)
    {
        $file = File::query()->where('id', $id)->with('tags')->first();
        $tags = Tag::query()->get();
        return view('editForm', compact(['file', 'tags']));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function doEdit(Request $request)
    {
        $file = $request->except('_token');
        $id = $file['id'];
        $des = $file['description'];
        $tags = $request->get('tags');
        $priority = $request->get('priority');
        // dd($tags);
//        File::query()->where('id', $id)->update(['description' => $des]);
        $newfile = File::find($id);
        foreach ($tags as $tag) {
            dd($newfile->tags()->sync([$tag - 1 => ['priority' => $priority[$tag - 1]]]));
        }

        return redirect()->route('gallery');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteFile($id)
    {

        File::query()->where('id', $id)->delete();
        return redirect()->route('gallery');
    }


    public function sendComment(Request $request)
    {
        $comment = $request->get('comment');

        $id = $request->get('id');
        Comment::query()->insert(['file_id' => $id, 'comment' => $comment]);
        return back();
    }

    /*   public function sendTag(Request $request)
       {
           $tags = $request->get('tags');
           //dd($tags);
           $file_id = $request->get('id');
          // $file=new File();
          $file= File::find($file_id);
           $file->tags()->sync($tags);

           //   dd($tags);
           return back();
       }*/

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showArray()
    {
        $users = [
            'a' => ['name' => 'Mohammad', 'age' => '33'],
            'b' => ['name' => 'ali', 'age' => 15]
        ];
        return view('form', ['users' => $users]);
    }

    public function downloadHtml()
    {
        $users = [
            'a' => ['name' => 'Mohammad', 'age' => '33'],
            'b' => ['name' => 'ali', 'age' => 15]
        ];
        $html = view('form', ['users' => $users])->render();
        Storage::put('output/file1.html', $html);
        return Storage::disk('local')->download('output/file1.html');
    }

    public function gallery(File $file)
    {

        if (!\Auth::user()->isAdmin || !$file->user_id == \Auth::id()) {
            return abort(403);
        }
        /*    $files = File::query()
                ->with('comments')->with('tags')
                ->when($request->has('hascomment'), function (\Illuminate\Database\Eloquent\Builder $query) {
                    $query->whereHas('comments', function (\Illuminate\Database\Eloquent\Builder $query) {
                        $query->where('created_at', '>', Carbon::yesterday());
                    });
                }
                )->get();*/

       // $file = File::query()->with('comments')->with('tags')->where('id', $id)->first();

        $tags = Tag::query()->get();
        return view('fileDetails', compact(['file', 'tags']));
        // return view('gallery', compact('files'));
    }
}
