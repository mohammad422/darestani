<?php

namespace App\Http\Controllers;

use Gumlet\ImageResize;
use Gumlet\ImageResizeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{

    /**
     * @param string $image
     * @param int|null $width
     * @param null $height
     * @return mixed
     * @throws ImageResizeException
     */
    public function imgResize(string $image, int $width = null, $height = null)
    {

        $img = explode('.', $image);
        $this->imgName($img[0]);
        $this->imgType($img[1]);
        $this->imgExist($image);
        $file = storage_path('/app/images/' . $image);
        $this->imageSize($image);

        if ($width && $height) {
            $image1 = new ImageResize($file);
            //$image1->crop($width, $height, true, ImageResize::CROPTOP);
            $image->resize($width, $height);
            $image1->save(storage_path('/app/resized-photos/') . $image);
            return Storage::download('/resized-photos/' . $image);
        }

        return Storage::download('/images/' . $image);

    }

    /**
     * @param $img
     */
    public function imgName($img)
    {
        if (!preg_match('/^[a-zA-Z]+$/', $img)) {
            die("Image Name Must Be Have Uppercase and Lowercase Letters");
        }
    }

    /**
     * @param $img
     */
    public function imgType($img)
    {
        $typeImg = Array(1 => 'jpg', 2 => 'jpeg', 3 => 'png');
        if (!in_array($img, $typeImg)) {
            die("Image type is not Acceptable");
        }

    }

    /**
     * @param $img
     */
    public function imgExist($img)
    {
        if (!Storage::exists('/images/' . $img)) {
            die("This Image is not Exist");
        }
    }

    /**
     * @param $img
     */
    public function imageSize($img)
    {
        $file = storage_path('/app/images/' . $img);

        $size = getimagesize($file);
        if ($size["0"] <= 50 || $size["0"] >= 150 && $size["1"] <= 50 || $size["1"] >= 150) {
            die("Image Size is not Between 50 and 150");
        }
    }

}
