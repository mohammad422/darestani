<?php

namespace App\Http\Controllers;

use App\Resume;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ResumeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('resume');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Request $request)
    {

        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'skill.*' => Rule::in(['Php', 'Javascript', 'Java', 'Python', 'Wordpress']),
            'file' => 'max:5000|required',
            'skill' => 'max:3'
        ]);
        $var = $request->input('skill');

        $attribute = $request->except(['_token', 'skill', 'file']);
        $attribute['skill'] = implode(',', $var);

        $attribute['fakename'] = $request->file('file')->store('upload');

        $attribute['file'] = $request->file('file')->getClientOriginalName();

        Resume::create($attribute);
         return back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAllResumes()
    {

        $resumes = Resume::query()->get();

        return view('resumes', compact('resumes'));
    }

    /**
     * @param $file
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadFile($file)
    {

        $path = storage_path('/app/upload/' . $file);
        return response()->download($path);
    }

}
