<?php

namespace App\Http\Controllers;

use Gumlet\ImageResize;
use Gumlet\ImageResizeException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use wapmorgan\UnifiedArchive\UnifiedArchive;

class UserController extends Controller
{

    /**
     * @return string
     */
    public function index()
    {
        return "<p style='margin-left: 120px; ;margin-top: 200px;font-size: 100px;'>User</p>";
    }

    /**
     * @param $user
     * @return string
     */
    public function action($user = null)
    {
        return "<p style='margin-left: 120px; ;margin-top: 200px;font-size: 100px;'>Hello $user</p>";
    }

    /**
     * @param $post1
     * @param $post2
     * @param null $post3
     * @return string
     */
    public function action2($post1, $post2, $post3 = null)
    {
        return "<p style='margin-left: 120px; ;margin-top: 200px;font-size: 100px;'> $post1 $post2 $post3</p>";
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function action3()
    {
        // dd(__DIR__);
        $root1 = storage_path();
        $root2 = storage_path('text.zip');
        $archive = UnifiedArchive::open($root2);
        $archive->extractFiles($root1 . '/output');
        return "Ok";
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Exception
     */
    public function extract(Request $request)
    {
//       dd(storage_path());
        $name = $request->input('name');
        $archive = UnifiedArchive::open(storage_path($name));
        $archive->extractFiles(storage_path() . "/output");
//        $archive = UnifiedArchive::open(storage_path() . "\\$name");
//         $archive->extractFiles(storage_path() . "\output");
        return "ok";
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function extract2(Request $request)
    {
        $name = $request->input('name');
        $path = storage_path($name);
        $archive = UnifiedArchive::open($path);
        $archive->extractFiles(storage_path() . "/app/output");
        $contents = Storage::get("/output/text.txt");
        dd($contents);
    }

    /**
     * @param Request $request
     * @throws \Exception
     */
    public function extract3(Request $request)
    {
        $name = $request->input('name');
        $txt_name = $request->input('txt_name');
        if (Storage::exists($name)) {
            $path = storage_path() . "/app/" . $name;
            $archive = UnifiedArchive::open($path);
            $archive->extractFiles(storage_path() . "/app/output");
            if (Storage::exists("/output/" . $txt_name)) {
                $contents = Storage::get("/output/" . $txt_name);
                dd($contents);
            } else {
                $contents = Storage::get("/output/index.txt");
                dd($contents);
            }
        } else {
            $contents = Storage::get("/output/index.txt");
            dd($contents);
        }

    }

    public function namedRoute()
    {
        return route('routename',['name'=>'mohammad','width'=>'300','height'=>'200','test'=>'myTest']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function langFa()
    {
        session()->put('key',['value1']);
        session()->push('key', 'developers');
        session()->push('key', 'developers1');
       // session()->pull('key', 'default');
        dd(session()->all());
        //$request->session()->push('key', 'var');
        // return trans('passwords.reset');
        return view('localization');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function setSession(Request $request)
    {
        $request->session()->flash('name','Alireza');
        return $request->session()->all();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getSession(Request $request)
    {
        return $request->session()->all();
    }
}
