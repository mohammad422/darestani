<?php

namespace App\Http\Controllers;

use App\Imports\UsersImport;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{

    public $ratios = [
        'e1' => 2,
        'e2' => 1,
        'e3' => 3,
        'e4' => 2,
        'e5' => 5,
        'e6' => 2,
        'e7' => 1,
        'e8' => 12
    ];

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function readFile()
    {
        $excelFile = storage_path('app/scores/scores.xlsx');

        $scoresList = Excel::toArray(new UsersImport, $excelFile);
      // dd($scoresList);
        $Final = Arr::first($scoresList);

        //dd($Final);
        $this->gpaCalculation($Final);
        $textFile = storage_path("app/scores/gpa.txt");

        return response()->download($textFile)->deleteFileAfterSend();
    }

    /**
     * @param array $students
     */
    public function gpaCalculation(array $students)
    {
        $sumScores = [];
        $list = '';
        foreach ($students as $student) {
            $name = array_shift($student);
            foreach ($student as $key => $value) {
                $sumScores[$key] = $value * $this->ratios[$key];
            }
            $gpa = round(array_sum($sumScores) / array_sum($this->ratios));
            $list .= $name . "GPA =" . $gpa . "\n";
        }
        $this->textFile($list);
    }

    public function textFile(string $scoresList)
    {
        Storage::disk('scores')->put('gpa.txt',$scoresList);
    }

}
