<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use PHPUnit\Util\RegularExpressionTest;

class Employee extends Model
{
    protected $table = 'employees';
    //public $timestamps = false;
    //protected $fillable = ['username','email','tel','password'];
    public $username;
    public $email;

    /*private  function __construct($username,$email)
    {
        $this->username = $username;
        $this->email = $email;
    }

    public static function find($id)
    {
        $employee = DB::table('employees')->select()->where('id','=',$id)->first();
        return new static($employee->username,$employee->email);
    }*/
    public function getTable()
    {
        return $this->table;
    }

    public function setAttribute($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    public static function find($id)
    {
        $employer = new static();
        $result = DB::table($employer->getTable())->where('id', '=', $id)->get()->first();

       $myEmployer = new Employee();
        foreach ($result as $key => $value) {
            $myEmployer->setAttribute($key, $value);
        }

        return $myEmployer;
    }
    public function getModel($result)
    {
        $myEmployer = new Employee();
        foreach ($result as $key => $value) {
            $myEmployer->setAttribute($key, $value);
        }
        return $myEmployer;
    }

    public static function all1()
    {
        $employers = new static();
         $result = DB::table($employers->getTable())->get();
        //$myEmployer = new Employee();
      //  dd($result);
        $items=[];
        foreach ($result as $key => $value) {
           $items[]=(new Employee())->getModel($value);
        }

        return $items;
    }


}
